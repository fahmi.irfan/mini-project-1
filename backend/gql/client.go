package gql

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"
)

type Client interface {
	Query(query string) (*http.Response, error)
	SetTimeout(time time.Duration)
	SetHeader(key, value string)
}

type client struct {
	headers    http.Header
	url        string
	httpClient *http.Client
	timeout    time.Duration
}

func NewGraphqlClient(url string) *client {
	return &client{
		headers:    make(http.Header),
		url:        url,
		httpClient: &http.Client{},
		timeout:    time.Duration(5 * time.Second),
	}
}

func (c *client) SetTimeout(time time.Duration) {
	c.timeout = time
}

func (c *client) SetHeader(key, value string) {
	c.headers.Set(key, value)
}

func (c *client) Query(query string) (*http.Response, error) {
	res := &http.Response{}
	q := struct {
		Query string `json:"query"`
	}{Query: query}

	queryData, err := json.Marshal(q)
	if err != nil {
		return res, err
	}

	req, err := http.NewRequest("POST", c.url, bytes.NewBuffer(queryData))
	if err != nil {
		return res, err
	}

	req.Header = c.headers
	res, err = c.httpClient.Do(req)
	if err != nil {
		return res, err
	}

	return res, nil
}
