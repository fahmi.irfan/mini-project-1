package main

import (
	"context"
	"log"

	"gitlab.com/fahmi.irfan/mini-project-1/issue_tracker/ghtracker"
	m "gitlab.com/fahmi.irfan/mini-project-1/issue_tracker/model"
)

type Resolver interface {
	Issues(ctx context.Context, last int) []m.Issue
}

type IResolver struct {
	issueTracker ghtracker.IssueTracker
}

func (r *IResolver) Issues() *[]*m.Issue {
	issues := make([]*m.Issue, 0)
	payload, err := r.issueTracker.Get(100)

	if err != nil {
		log.Println(err)
		return &issues
	}

	for _, i := range payload.Data.Repository.Issues.Nodes {
		issue := m.Issue{}
		issue.MComments = i.Comments.Nodes
		issue.MBodyHTML = i.Body
		issue.MNumber = i.Number
		issue.MTitle = i.Title

		issues = append(issues, &issue)
	}

	return &issues
}
