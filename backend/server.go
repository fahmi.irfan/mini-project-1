package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/fahmi.irfan/mini-project-1/issue_tracker/ghtracker"
	"gitlab.com/fahmi.irfan/mini-project-1/issue_tracker/gql"

	"github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/joho/godotenv"
	"github.com/urfave/negroni"
)

// Endpoint of github api
var Endpoint = "https://api.github.com/graphql"
var token = "Bearer " + os.Getenv("GITHUB_PRIVATE_KEY")

func init() {
	if err := godotenv.Load(); err != nil {
		log.Println(err)
	}

	ghToken := os.Getenv("GITHUB_PRIVATE_KEY")
	token = "Bearer " + ghToken
}

func main() {
	gqlClient := gql.NewGraphqlClient(Endpoint)
	gqlClient.SetHeader("Authorization", token)
	gqlClient.SetHeader("Content-Type", "application/json")

	issueTracker := ghtracker.IIssueTracker{
		URL:       Endpoint,
		Token:     token,
		GqlClient: gqlClient,
	}

	s, err := getSchema("./schema.graphql")
	if err != nil {
		log.Fatal(err)
	}

	resolver := IResolver{
		issueTracker: &issueTracker,
	}

	schema := graphql.MustParseSchema(s, &resolver)
	mux := http.NewServeMux()
	mux.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write(page)
	}))
	mux.Handle("/api/graphql", &relay.Handler{Schema: schema})

	n := negroni.Classic() // Includes some default middlewares
	n.UseHandler(mux)

	log.Println(">>> server run on http://localhost:8000")
	log.Fatal(http.ListenAndServe(":8000", n))
	log.Println(">>> exited...")
}

var page = []byte(`
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/graphiql/0.10.2/graphiql.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/fetch/1.1.0/fetch.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.5.4/react.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.5.4/react-dom.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/graphiql/0.10.2/graphiql.js"></script>
	</head>
	<body style="width: 100%; height: 100%; margin: 0; overflow: hidden;">
		<div id="graphiql" style="height: 100vh;">Loading...</div>
		<script>
			function graphQLFetcher(graphQLParams) {
				return fetch("/api/graphql", {
					method: "post",
					body: JSON.stringify(graphQLParams),
					credentials: "include",
				}).then(function (response) {
					return response.text();
				}).then(function (responseBody) {
					try {
						return JSON.parse(responseBody);
					} catch (error) {
						return responseBody;
					}
				});
			}
			ReactDOM.render(
				React.createElement(GraphiQL, {fetcher: graphQLFetcher}),
				document.getElementById("graphiql")
			);
		</script>
	</body>
</html>
`)

func getSchema(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(b), nil
}
