module gitlab.com/fahmi.irfan/mini-project-1/issue_tracker

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/graph-gophers/graphql-go v0.0.0-20190610161739-8f92f34fc598
	github.com/joho/godotenv v1.3.0
	github.com/urfave/negroni v1.0.0
)
