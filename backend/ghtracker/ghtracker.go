package ghtracker

import (
	"encoding/json"
	"fmt"

	"gitlab.com/fahmi.irfan/mini-project-1/issue_tracker/gql"
	m "gitlab.com/fahmi.irfan/mini-project-1/issue_tracker/model"
)

// Payload github issues grapqhql payload
type Payload struct {
	Data struct {
		Repository struct {
			Issues struct {
				Nodes []struct {
					Title    string `json:"title"`
					Body     string `json:"bodyHTML"`
					Number   int32  `json:"number"`
					Comments struct {
						Nodes []m.Comment `json:"nodes"`
					} `json:"comments"`
				} `json:"nodes"`
			} `json:"issues"`
		} `json:"repository"`
	} `json:"data"`
}

// IssueTracker interface
type IssueTracker interface {
	Get(last int32) (Payload, error)
}

// IIssueTracker implements IssueTracker
type IIssueTracker struct {
	URL       string
	Token     string
	GqlClient gql.Client
}

// Get return the `last` latest issues
func (i *IIssueTracker) Get(last int32) (Payload, error) {
	payload := Payload{}
	res, err := i.GqlClient.Query(githubIssues("facebook", "react", last))
	if err != nil {
		return payload, err
	}
	defer res.Body.Close()

	json.NewDecoder(res.Body).Decode(&payload)
	return payload, nil
}

func githubIssues(owner, projName string, last int32) string {
	query := fmt.Sprintf(`query {
		repository(owner: "%s", name: "%s") {
			issues (last: %d){
				nodes {
					number
					title 
					bodyHTML 
					comments (last: %d) {
						nodes {
							author {
								avatarUrl
								url
								login
							}
							bodyHTML
							body
						}
					}
				}
			}
		}
	}
	`, owner, projName, last, last)

	return query
}
