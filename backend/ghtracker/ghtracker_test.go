package ghtracker

import (
	"log"
	"os"
	"reflect"
	"testing"

	"github.com/joho/godotenv"
	"gitlab.com/fahmi.irfan/mini-project-1/issue_tracker/gql"
)

// Endpoint of github api
var Endpoint = "https://api.github.com/graphql"
var token = "Bearer " + os.Getenv("GITHUB_PRIVATE_KEY")

func init() {
	if err := godotenv.Load(); err != nil {
		log.Println(err)
	}

	ghToken := os.Getenv("GITHUB_PRIVATE_KEY")
	token = "Bearer " + ghToken
}

func TestGetIssues(t *testing.T) {
	gqlClient := gql.NewGraphqlClient(Endpoint)
	gqlClient.SetHeader("Authorization", token)
	gqlClient.SetHeader("Content-Type", "application/json")

	t.Run("got no error", func(t *testing.T) {
		issues := IIssueTracker{
			URL:       Endpoint,
			Token:     token,
			GqlClient: gqlClient,
		}

		_, err := issues.Get(50)
		if err != nil {
			t.Errorf("want no error, got %v", err)
		}
	})

	t.Run("got the payload", func(t *testing.T) {
		issue := IIssueTracker{
			URL:       Endpoint,
			Token:     token,
			GqlClient: gqlClient,
		}

		got, err := issue.Get(50)
		want := Payload{}

		if err != nil {
			t.Errorf("want no error, got %v", err)
		}

		if reflect.DeepEqual(got, want) {
			t.Errorf("got empty json %v", got)
		}
	})
}
