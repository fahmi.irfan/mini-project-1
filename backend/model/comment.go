package model

type Comment struct {
	MAuthor   Author `json:"author"`
	MBodyHTML string `json:"bodyHTML"`
}

func (c *Comment) Author() *Author {
	return &c.MAuthor
}

func (c *Comment) BodyHTML() *string {
	return &c.MBodyHTML
}
