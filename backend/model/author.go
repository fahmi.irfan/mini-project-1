package model

type Author struct {
	MLogin     string `json:"login"`
	MAvatarURL string `json:"avatarUrl"`
	MURL       string `json:"url"`
}

func (a *Author) Login() *string {
	return &a.MLogin
}

func (a *Author) AvatarURL() *string {
	return &a.MAvatarURL
}

func (a *Author) URL() *string {
	return &a.MURL
}
