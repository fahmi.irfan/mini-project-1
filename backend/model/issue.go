package model

type Issue struct {
	MTitle    string    `json:"title"`
	MBodyHTML string    `json:"bodyHTML"`
	MNumber   int32     `json:"number"`
	MComments []Comment `json:"comments"`
}

func (i *Issue) Title() *string {
	return &i.MTitle
}

func (i *Issue) BodyHTMl() *string {
	return &i.MBodyHTML
}

func (i *Issue) Number() *int32 {
	return &i.MNumber
}

func (i *Issue) Comments() *[]*Comment {
	var comms []*Comment
	for _, comm := range i.MComments {
		c := comm
		comms = append(comms, &c)
	}

	return &comms
}
