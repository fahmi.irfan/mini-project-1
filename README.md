# Mini Project 1
This mini project for kumparan academy bootcamp.

## Requirement
- [x] user can see facebook/reactjs github issues, that consists of:
    - [x] issue ticket number
    - [x] issue title
- [x] user can see details of particular issue by clicking on it. The details consists of:
    - issue ticket number
    - issue title
    - issue creator name
    - [x] listing comments:
        - commentator's names
        - comments body

## Services
- backend: Graphql server Golang
- frontend: Nodejs (proxy) + Vue

## Build docker image
- backend
    - `cd` to backend then `make build`
- frontend
    - `cd` to frontend then `make build`

## Data flow
- `frontend client (browser) <-- frontend server (nodejs) <-- graphql service <-- github graphql service`