FROM node:alpine
WORKDIR /app
COPY package*.json ./
RUN npm install --production
COPY . /app
CMD [ "npm",  "run", "start-prod" ]
