const express = require('express')
const path = require('path')
const axios = require('axios')

const app = express()

const cfg = {
    PORT: process.env.PORT || 3000,
    GHTRACKER_ADDR: process.env.GHTRACKER_ADDR || 'http://localhost:8000'
}

app.use('/', express.static(path.join(__dirname, 'static')))
app.get('/api/ghtracker', async (req, res) => {
    const query = `{
        issues {
          number
          title
          bodyHtml
          comments {
            bodyHtml
            author {
              avatarUrl
              login
              url
            }
          }
        }
      }`

    try {
        const { data } = await axios.post(`${cfg.GHTRACKER_ADDR}/api/graphql`, {
            query,	
        })
        return res.status(200).json(data)
    } catch (e) {
        console.error(e);
        return res.status(400).json({ error: e.message })
    }

})
app.listen(cfg.PORT)
console.log(`>>> server listening on :${cfg.PORT}`)